# README #

Contains program and slides from previous PETSc meetings as well as information about future planned meetings.

The material is presented at www.mcs.anl.gov/petsc/meetings

To update the data as the petsc user do the following:

```
cd /mcs/www.mcs.anl.gov/research/projects/petsc/meetings
git pull
```
